from huggingface_hub import hf_hub_download
## Define model name and file name
model_name = "TheBloke/CodeLlama-7B-Instruct-GGUF"
model_file = "codellama-7b-instruct.Q2_K.gguf"

model_path = hf_hub_download(model_name, filename=model_file, cache_dir = "./models")
with open('Modelfile', 'w') as file:
    file.write(f"FROM {model_path}")

