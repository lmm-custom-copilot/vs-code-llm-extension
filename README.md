# vs-code-llm-extension

## Task Aim: Setting Up a Tool (Assistant) for Writing Code Using LLM (Large Language Models)

There are several possibilities for achieving this goal:

1) Download selected LLM files and run them as a server using available libraries like Ollama or llama-cpp-python[server], along with existing plugins (e.g., Continue for Visual Studio Code).
2) Download selected LLM files and run them as a server using available libraries like Ollama or llama-cpp-python[server], and develop your own plugin for an IDE compatible with the exposed API.
3) Download selected LLM files and create own serwer side applicaction that allows to interact with model through API endpoints (based on API from one of providers for example OpeAI), along with existing plugins (e.g., Continue for Visual Studio Code).
4) Develop your own application - code assistant. Initially, I created a code assistant chat because I misunderstood the given task a bit. As a developer, I almost always want to write a piece of code. Therefore, I created a code assistant chat that allows playing with some parameters passed to LLM with each request.

## Main Task Solution: Setting Up a Tool

Since creating a full solution from scratch, including an API and compatible IDE plugin, could take too much time, I decided to explore existing solutions. In my first attempt, I chose to set up the lightest version of "TheBloke/CodeLlama-7B-Instruct-GGUF" as my local LLM copilot.

### 1st Step:
Initially, I tried to set up the coding assistant on a MacBook Pro with an M3 chip. I chose Ollama to expose the locally downloaded LLM because it is more suitable for Macbooks and allows using both CPU and GPU by default.

To expose our locally downloaded LLM using Ollama, we need to download Ollama from [ollama.com](https://ollama.com/). Then, we can pull some models available in the Ollama library. In our case, I needed the smallest possible version of Codellama, which is already quantized and can be found in the [repository](https://huggingface.co/TheBloke/CodeLlama-7B-Instruct-GGUF).

To speed up the process, I created a simple Python script, `download_model.py`, which downloads the chosen model from Hugging Face and stores it locally. Additionally, the script generates a `Modelfile` required to add this local model to the available models through Ollama.

To add the local model from the file to Ollama, we need to run the command:
```
ollama create <model_name> -f Modelfile
```
After that, our model will be accessible through the API by running:
```
ollama serve
```
By default, the model's API is served at `http://localhost:11434`. Once our local server is up and running, we can proceed to the next step.

### 2nd Step:
In the second step, we need a compatible plugin that allows our IDE (Visual Studio Code) to interact with our locally served LLMs. For this purpose, I chose the open-source plugin, Continue ([continue.dev](https://continue.dev/)).

After installation, we need to slightly modify the default configuration file (`config.json`), which can be found through plugin settings or under the directory (for Mac), in my case `/Users/emilia/.continue/config.json`.

We need to manually add our model to the `"models": []` list:
```json
{
  "model": "codellama-7b-instruct",
  "title": "codellama-7b-instruct",
  "completionOptions": {},
  "apiBase": "http://localhost:11434",
  "provider": "ollama"
}
```
Alternatively, from the level of the Continue plugin, we can find the autodetect option:
```json
{
  "model": "AUTODETECT",
  "title": "Ollama",
  "completionOptions": {},
  "apiBase": "http://localhost:11434",
  "provider": "ollama"
}
```
After this setup, we should have our local copilot up and running with locally hosted LLM.

### FUTURE DEVELOPMENT
 >#### this little task inpired me to create own VS Code plugin with locally served LLM throgh own API endpoints. Keep eye to my repositories in the nearest future if you are interested
 >### Next Steps:
 >#### next step of development will include point number 3 from previous description. I will try to expose locally hosted LLM as an API compatible with OpenAI API schema an use it together with Continue plugin. You can watch my progress ([>>>HERE<<<](https://gitlab.com/lmm-custom-copilot/backend/custom-copilot-api)).
